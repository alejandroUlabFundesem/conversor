<?php

$json = json_decode(file_get_contents('php://input'), true);

$respuesta = [ 'data' => "Bienvenido " . $json['nombre'] . " " . $json['apellido'] ];
header('Content-Type: application/json');
echo json_encode($respuesta);