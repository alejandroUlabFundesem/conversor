'use strict';

import { Conversor } from './Conversor.js';

(function () {

    window.addEventListener('DOMContentLoaded', function () {
        let conversor = new Conversor();
        conversor.iniciaEventos();
    });
    
})();