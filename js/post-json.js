'use strict';

(function () {

    window.addEventListener('DOMContentLoaded', function () {
        let profesor = {
            nombre: 'Juan', 
            apellido: 'Pérez'
        };

        let mensajeBienvenida;
        console.log("Voy a enviar datos");
        var http = new XMLHttpRequest(); 
        http.open('POST', 'http://localhost:90/conversor/index.php', true); 
        http.setRequestHeader("Content-type", "application/json"); 
        http.send(JSON.stringify(profesor)); 
        http.addEventListener('readystatechange', function() { 
            if(http.readyState === 4 && http.status === 200) { 
                console.log("datos recibidos");
                let respuesta = JSON.parse(http.responseText).data;
                mensajeBienvenida = respuesta;
                console.log(respuesta);
            }
        });
    });
    
})();