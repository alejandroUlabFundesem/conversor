'use strict';

export class Conversor {
    #mensajes;
    #importe;
    #URL_API;
    #moneda;

    constructor() {
        this.#mensajes = document.getElementById('mensajes');
        this.#importe = document.getElementById('importe');
        this.#URL_API = 'https://api.exchangeratesapi.io/latest';
    }

    obtenerCambiosConRespectoAlEuro() {
        let that = this;
        return new Promise(function(resolve, reject) {
            let http = new XMLHttpRequest();
            http.open('GET', that.#URL_API, true); 
            http.send(); 
            http.addEventListener('readystatechange', function() {
                if(http.readyState === 4 && http.status === 200) {
                    resolve(JSON.parse(http.responseText).rates);
                }                
            });
        });
    }

    convertirImporte() {
        let importe = this.#importe.value;

        importe = Number(importe);

        if (isNaN(importe) === true)
            this.#mensajes.innerHTML = "El importe tiene que ser numérico";
        else
        {
            let that = this;
            this.obtenerCambiosConRespectoAlEuro().then(function (rates) {
                let importeEnDolares = Number(that.#importe.value) * rates.USD;
                let importeEnLibras = Number(that.#importe.value) * rates.GBP;
                let importeenDolaresCanadienses = Number(that.#importe.value) * rates.CAD;
            
                that.#mensajes.innerHTML = "Importe en dolares: " + importeEnDolares.toLocaleString("en-US", { style: 'currency', currency: 'USD' }) + "<br>";
                that.#mensajes.innerHTML += "Importe en libras: " + importeEnLibras + "<br>";
                that.#mensajes.innerHTML += "Importe en dolares canadienses: " + importeenDolaresCanadienses + "<br>";
            }).catch(function (error) {
                
            });
        }
    }

    iniciaEventos () {
        let that = this;
        let btConvertir = document.getElementById('convertir');

        btConvertir.addEventListener('click', function () {
            that.convertirImporte();
        })
    }
}