'use strict';

(function () {

    let getUsuarios = () => {
        return new Promise(function(resolve, reject) {
            let http = new XMLHttpRequest();
            http.open('GET', 'https://reqres.in/api/users?page=1', true); 
            http.send(); 
            http.addEventListener('readystatechange', function() {
                if(http.readyState === 4 && http.status === 200) {
                    resolve(JSON.parse(http.responseText).data);
                }                
            });
        });        
    };

    window.addEventListener('DOMContentLoaded', function () {
        let botonCrear = document.getElementById("crear");

        botonCrear.addEventListener("click", function () {
            let usuario = {};
            usuario.name = document.getElementById('nombre').value;
            usuario.job = document.getElementById('trabajo').value;

            fetch('https://reqres.in/api/users', {
                method: 'POST',
                body: JSON.stringify(usuario),
                headers:{ 'Content-Type': 'application/json' }
            }).then((response) => response.json())
            .then(usuarioCreado => mensajes.innerHTML = JSON.stringify(usuarioCreado));
        });

        getUsuarios().then(usuarios => mensajes.innerHTML = JSON.stringify(usuarios));
    });

    
})();